package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.BlogEntity;
import com.example.cozastore22.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceV2 {
    @Autowired
    private BlogRepository blogRepository;

    public List<BlogEntity> getAllBlogs() {
        return blogRepository.findAll();
    }

    public BlogEntity getBlogById(Long id) {
        return blogRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Blog not found with id: " + id));
    }

    public BlogEntity createBlog(BlogEntity blog) {
        return blogRepository.save(blog);
    }

    public BlogEntity updateBlog(Long id, BlogEntity updatedBlog) {
        return blogRepository.findById(id).map(blog -> {
            blog.setTitle(updatedBlog.getTitle());
            blog.setContent(updatedBlog.getContent());
            blog.setImage(updatedBlog.getImage());
            blog.setCreateDate(updatedBlog.getCreateDate());
            blog.setUserEntity(updatedBlog.getUserEntity());
            blog.setTags(updatedBlog.getTags());
            blog.setEmail(updatedBlog.getEmail());
            return blogRepository.save(blog);
        }).orElseThrow(() -> new RuntimeException("Blog not found with id: " + id));
    }

    public void deleteBlog(Long id) {
        if (blogRepository.existsById(id)) {
            blogRepository.deleteById(id);
        } else {
            throw new RuntimeException("Blog not found with id: " + id);
        }
    }
}
