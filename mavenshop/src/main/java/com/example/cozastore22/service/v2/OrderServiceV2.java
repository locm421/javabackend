package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.OrderEntity;
import com.example.cozastore22.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceV2 {

    @Autowired
    private OrderRepository orderRepository;

    public List<OrderEntity> getAllOrders() {
        return orderRepository.findAll();
    }

    public OrderEntity getOrderById(Long id) {
        return orderRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Order not found with id: " + id));
    }

    public OrderEntity createOrder(OrderEntity order) {
        // Add any additional logic before saving, if needed
        return orderRepository.save(order);
    }

    public OrderEntity updateOrder(Long id, OrderEntity updatedOrder) {
        return orderRepository.findById(id).map(order -> {
            order.setUserEntity(updatedOrder.getUserEntity());
            order.setTotalPrice(updatedOrder.getTotalPrice());
            order.setCreateDate(updatedOrder.getCreateDate());
            // Update other properties as needed
            return orderRepository.save(order);
        }).orElseThrow(() -> new RuntimeException("Order not found with id: " + id));
    }

    public void deleteOrder(Long id) {
        if (orderRepository.existsById(id)) {
            orderRepository.deleteById(id);
        } else {
            throw new RuntimeException("Order not found with id: " + id);
        }
    }
}
