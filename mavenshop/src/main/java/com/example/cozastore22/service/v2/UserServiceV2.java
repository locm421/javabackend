package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.UserEntity;
import com.example.cozastore22.payload.response.UserResponse;
import com.example.cozastore22.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceV2 {

    @Autowired
    private UserRepository userRepository;

    public List<UserResponse> getAllUsers() {
        List<UserResponse> userResponseList = new ArrayList<>();
        List<UserEntity> list = userRepository.findAll();
        for(UserEntity item : list)
        {
            UserResponse userResponse = new UserResponse();
            userResponse.setId(item.getId());
            userResponse.setEmail(item.getEmail());
            userResponse.setPassword(item.getEmail());
            userResponseList.add(userResponse);
        }
        return userResponseList;
    }

    public UserEntity getUserById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found with id: " + id));
    }

    public UserEntity createUser(UserEntity user) {
        // Add any additional logic before saving, if needed
        return userRepository.save(user);
    }

    public UserEntity updateUser(int id, UserEntity updatedUser) {
        return userRepository.findById(id).map(user -> {
            user.setEmail(updatedUser.getEmail());
            user.setPassword(updatedUser.getPassword());
            user.setRole(updatedUser.getRole());
            // Update other properties as needed
            return userRepository.save(user);
        }).orElseThrow(() -> new RuntimeException("User not found with id: " + id));
    }

    public void deleteUser(int id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            throw new RuntimeException("User not found with id: " + id);
        }
    }
}
