package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.RoleEntity;
import com.example.cozastore22.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceV2 {

    @Autowired
    private RoleRepository roleRepository;

    public List<RoleEntity> getAllRoles() {
        return roleRepository.findAll();
    }

    public RoleEntity getRoleById(int id) {
        return roleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Role not found with id: " + id));
    }

    public RoleEntity createRole(RoleEntity role) {
        // Add any additional logic before saving, if needed
        return roleRepository.save(role);
    }

    public RoleEntity updateRole(int id, RoleEntity updatedRole) {
        return roleRepository.findById(id).map(role -> {
            role.setName(updatedRole.getName());
            // Update other properties as needed
            return roleRepository.save(role);
        }).orElseThrow(() -> new RuntimeException("Role not found with id: " + id));
    }

    public void deleteRole(int id) {
        if (roleRepository.existsById(id)) {
            roleRepository.deleteById(id);
        } else {
            throw new RuntimeException("Role not found with id: " + id);
        }
    }
}
