package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.CategoryEntity;
import com.example.cozastore22.payload.response.CategoryResponse;
import com.example.cozastore22.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceV2 {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<CategoryEntity> getAllCategories() {
        return categoryRepository.findAll();
    }

    public CategoryEntity getCategoryById(int id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Category not found with id: " + id));
    }
    public String getNameCategoryById(int id){
        Optional<CategoryEntity> categoryOptional = categoryRepository.findById(id);

        if (categoryOptional.isPresent()) {
            CategoryEntity category = categoryOptional.get();
            return category.getName();
        } else {
            // Xử lý khi không tìm thấy Category với id tương ứng
            return "Category not found";
        }
    }
    public CategoryEntity createCategory(CategoryEntity category) {
        // Add any additional logic before saving, if needed
        return categoryRepository.save(category);
    }

    public CategoryEntity updateCategory(int id, CategoryEntity updatedCategory) {
        return categoryRepository.findById(id).map(category -> {
            category.setName(updatedCategory.getName());
            // Update other properties as needed
            return categoryRepository.save(category);
        }).orElseThrow(() -> new RuntimeException("Category not found with id: " + id));
    }

    public void deleteCategory(int id) {
        if (categoryRepository.existsById(id)) {
            categoryRepository.deleteById(id);
        } else {
            throw new RuntimeException("Category not found with id: " + id);
        }
    }
}
