package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.ProductDetailEntity;
import com.example.cozastore22.repository.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductDetailServiceV2 {

    @Autowired
    private ProductDetailRepository productDetailRepository;

    public List<ProductDetailEntity> getAllProductDetails() {
        return productDetailRepository.findAll();
    }

    public ProductDetailEntity getProductDetailById(int id) {
        return productDetailRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("ProductDetail not found with id: " + id));
    }

    public ProductDetailEntity createProductDetail(ProductDetailEntity productDetail) {
        // Add any additional logic before saving, if needed
        return productDetailRepository.save(productDetail);
    }

    public ProductDetailEntity updateProductDetail(int id, ProductDetailEntity updatedProductDetail) {
        return productDetailRepository.findById(id).map(productDetail -> {
            productDetail.setQuantity(updatedProductDetail.getQuantity());
            productDetail.setDesc(updatedProductDetail.getDesc());
            productDetail.setColor(updatedProductDetail.getColor());
            // Update other properties as needed
            return productDetailRepository.save(productDetail);
        }).orElseThrow(() -> new RuntimeException("ProductDetail not found with id: " + id));
    }

    public void deleteProductDetail(int id) {
        if (productDetailRepository.existsById(id)) {
            productDetailRepository.deleteById(id);
        } else {
            throw new RuntimeException("ProductDetail not found with id: " + id);
        }
    }
}
