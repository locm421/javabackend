package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.CategoryEntity;
import com.example.cozastore22.entity.ProductEntity;
import com.example.cozastore22.payload.response.ProductResponse;
import com.example.cozastore22.repository.CategoryRepository;
import com.example.cozastore22.repository.ProductRespository;
import com.example.cozastore22.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceV2 {
    private Logger logger = LoggerFactory.getLogger(ProductServiceV2.class);


    @Autowired
    private ProductRespository productRespository;

    @Autowired
    CategoryServiceV2 categoryServiceV2;

    @Autowired
    CategoryRepository categoryRepository;
    public List<ProductResponse> getAllProducts() {
        List<ProductResponse> responseList = new ArrayList<>();
        List<ProductEntity> list = productRespository.findAll();


        for(ProductEntity item : list)
        {

            ProductResponse response = new ProductResponse();
            response.setId(item.getId());
            response.setPrice(item.getPrice());
            response.setTitle(item.getTitle());
            response.setImage_url(item.getImages());
            System.out.println("product kiem tra id_category " + item.getCategory());
            String name = categoryServiceV2.getNameCategoryById(item.getId());
            System.out.println("hết đi má " + name);
            response.setCategory(name);
            responseList.add(response);
        }
        return responseList;
    }

    public ProductEntity getProductById(int id) {
        return productRespository.findById(id)
                .orElseThrow(() -> new RuntimeException("Product not found with id: " + id));
    }
    public List<ProductResponse> getAllProductCategory(int id) {
        List<ProductResponse> responseList = new ArrayList<>();
        List<ProductEntity> list = productRespository.findByCategory_Id(id);


        for(ProductEntity item : list)
        {

            ProductResponse response = new ProductResponse();
            response.setId(item.getId());
            response.setPrice(item.getPrice());
            response.setTitle(item.getTitle());
            response.setImage_url(item.getImages());
            System.out.println("product kiem tra id_category " + item.getId());
            String name = categoryServiceV2.getNameCategoryById(3);
            System.out.println("hết đi má " + name);
            response.setCategory(name);
            responseList.add(response);
        }
        return responseList;
    }
    public boolean insertProduct(String title, double prices, int idCategory, String image_url) {




        CategoryEntity category = new CategoryEntity();
        ProductEntity product = new ProductEntity();
        product.setPrice(prices);
        product.setTitle(title);
        product.setImages(image_url);
        category.setId(idCategory);
        product.setCategory(category);
        try{
            productRespository.save(product);
        }catch(Exception e){
            System.out.println("lỗi insert product " + e.getLocalizedMessage());
        }

        return true;
    }

    public ProductEntity updateProduct(int id, ProductEntity updatedProduct) {
        return productRespository.findById(id).map(product -> {
            product.setImages(updatedProduct.getImages());
            product.setTitle(updatedProduct.getTitle());
            product.setPrice(updatedProduct.getPrice());
            product.setTags(updatedProduct.getTags());
            product.setCategory(updatedProduct.getCategory());
            // Update other properties as needed
            return productRespository.save(product);
        }).orElseThrow(() -> new RuntimeException("Product not found with id: " + id));
    }

    public void deleteProduct(int id) {
        if (productRespository.existsById(id)) {
            productRespository.deleteById(id);
        } else {
            throw new RuntimeException("Product not found with id: " + id);
        }
    }
}
