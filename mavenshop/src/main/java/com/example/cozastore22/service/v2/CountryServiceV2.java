package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.CountryEntity;
import com.example.cozastore22.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceV2 {

    @Autowired
    private CountryRepository countryRepository;

    public List<CountryEntity> getAllCountries() {
        return countryRepository.findAll();
    }

    public CountryEntity getCountryById(Long id) {
        return countryRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Country not found with id: " + id));
    }

    public CountryEntity createCountry(CountryEntity country) {
        // Add any additional logic before saving, if needed
        return countryRepository.save(country);
    }

    public CountryEntity updateCountry(Long id, CountryEntity updatedCountry) {
        return countryRepository.findById(id).map(country -> {
            country.setName(updatedCountry.getName());
            // Update other properties as needed
            return countryRepository.save(country);
        }).orElseThrow(() -> new RuntimeException("Country not found with id: " + id));
    }

    public void deleteCountry(Long id) {
        if (countryRepository.existsById(id)) {
            countryRepository.deleteById(id);
        } else {
            throw new RuntimeException("Country not found with id: " + id);
        }
    }
}