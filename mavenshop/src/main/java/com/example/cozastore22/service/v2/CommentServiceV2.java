package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.CommentEntity;
import com.example.cozastore22.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceV2 {

    @Autowired
    private CommentRepository commentRepository;

    public List<CommentEntity> getAllComments() {
        return commentRepository.findAll();
    }

    public CommentEntity getCommentById(Long id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Comment not found with id: " + id));
    }

    public CommentEntity createComment(CommentEntity comment) {
        // Add any additional logic before saving, if needed
        return commentRepository.save(comment);
    }

    public CommentEntity updateComment(Long id, CommentEntity updatedComment) {
        return commentRepository.findById(id).map(comment -> {
            comment.setName(updatedComment.getName());
            comment.setContent(updatedComment.getContent());
            comment.setEmail(updatedComment.getEmail());
            comment.setWebsite(updatedComment.getWebsite());
            // Update other properties as needed
            return commentRepository.save(comment);
        }).orElseThrow(() -> new RuntimeException("Comment not found with id: " + id));
    }

    public void deleteComment(Long id) {
        if (commentRepository.existsById(id)) {
            commentRepository.deleteById(id);
        } else {
            throw new RuntimeException("Comment not found with id: " + id);
        }
    }
}
