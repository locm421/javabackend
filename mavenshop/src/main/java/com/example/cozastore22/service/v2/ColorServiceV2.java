package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.ColorEntity;
import com.example.cozastore22.repository.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorServiceV2 {

    @Autowired
    private ColorRepository colorRepository;

    public List<ColorEntity> getAllColors() {
        return colorRepository.findAll();
    }

    public ColorEntity getColorById(int id) {
        return colorRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Color not found with id: " + id));
    }

    public ColorEntity createColor(ColorEntity color) {
        // Add any additional logic before saving, if needed
        return colorRepository.save(color);
    }

    public ColorEntity updateColor(int id, ColorEntity updatedColor) {
        return colorRepository.findById(id).map(color -> {
            color.setName(updatedColor.getName());
            // Update other properties as needed
            return colorRepository.save(color);
        }).orElseThrow(() -> new RuntimeException("Color not found with id: " + id));
    }

    public void deleteColor(int id) {
        if (colorRepository.existsById(id)) {
            colorRepository.deleteById(id);
        } else {
            throw new RuntimeException("Color not found with id: " + id);
        }
    }
}
