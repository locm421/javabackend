package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.SizeEntity;
import com.example.cozastore22.repository.SizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SizeServiceV2 {

    @Autowired
    private SizeRepository sizeRepository;

    public List<SizeEntity> getAllSizes() {
        return sizeRepository.findAll();
    }

    public SizeEntity getSizeById(int id) {
        return sizeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Size not found with id: " + id));
    }

    public SizeEntity createSize(SizeEntity size) {
        // Add any additional logic before saving, if needed
        return sizeRepository.save(size);
    }

    public SizeEntity updateSize(int id, SizeEntity updatedSize) {
        return sizeRepository.findById(id).map(size -> {
            size.setName(updatedSize.getName());
            // Update other properties as needed
            return sizeRepository.save(size);
        }).orElseThrow(() -> new RuntimeException("Size not found with id: " + id));
    }

    public void deleteSize(int id) {
        if (sizeRepository.existsById(id)) {
            sizeRepository.deleteById(id);
        } else {
            throw new RuntimeException("Size not found with id: " + id);
        }
    }
}
