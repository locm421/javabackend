package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.OrderDetailEntity;
import com.example.cozastore22.repository.OrderDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceV2 {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    public List<OrderDetailEntity> getAllOrderDetails() {
        return orderDetailRepository.findAll();
    }

    public OrderDetailEntity getOrderDetailById(Long id) {
        return orderDetailRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("OrderDetail not found with id: " + id));
    }

    public OrderDetailEntity createOrderDetail(OrderDetailEntity orderDetail) {
        // Add any additional logic before saving, if needed
        return orderDetailRepository.save(orderDetail);
    }

    public OrderDetailEntity updateOrderDetail(Long id, OrderDetailEntity updatedOrderDetail) {
        return orderDetailRepository.findById(id).map(orderDetail -> {
            orderDetail.setUserEntity(updatedOrderDetail.getUserEntity());
            orderDetail.setProductDetailEntity(updatedOrderDetail.getProductDetailEntity());
            orderDetail.setPrice(updatedOrderDetail.getPrice());
            orderDetail.setQuantity(updatedOrderDetail.getQuantity());
            orderDetail.setCreateDate(updatedOrderDetail.getCreateDate());
            // Update other properties as needed
            return orderDetailRepository.save(orderDetail);
        }).orElseThrow(() -> new RuntimeException("OrderDetail not found with id: " + id));
    }

    public void deleteOrderDetail(Long id) {
        if (orderDetailRepository.existsById(id)) {
            orderDetailRepository.deleteById(id);
        } else {
            throw new RuntimeException("OrderDetail not found with id: " + id);
        }
    }
}
