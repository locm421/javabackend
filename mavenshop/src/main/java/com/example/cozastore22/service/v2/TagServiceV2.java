package com.example.cozastore22.service.v2;

import com.example.cozastore22.entity.TagEntity;
import com.example.cozastore22.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceV2 {

    @Autowired
    private TagRepository tagRepository;

    public List<TagEntity> getAllTags() {
        return tagRepository.findAll();
    }

    public TagEntity getTagById(Long id) {
        return tagRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Tag not found with id: " + id));
    }

    public TagEntity createTag(TagEntity tag) {
        // Add any additional logic before saving, if needed
        return tagRepository.save(tag);
    }

    public TagEntity updateTag(Long id, TagEntity updatedTag) {
        return tagRepository.findById(id).map(tag -> {
            tag.setName(updatedTag.getName());
            // Update other properties as needed
            return tagRepository.save(tag);
        }).orElseThrow(() -> new RuntimeException("Tag not found with id: " + id));
    }

    public void deleteTag(Long id) {
        if (tagRepository.existsById(id)) {
            tagRepository.deleteById(id);
        } else {
            throw new RuntimeException("Tag not found with id: " + id);
        }
    }
}
