package com.example.cozastore22.service.imp;

import com.example.cozastore22.payload.response.ProductResponse;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Repository
public interface ProducServiceImp {
    boolean insertProduct(String title, double prices, int idCategory);
    List<ProductResponse> getAllProducts();
    Resource downloadProductFile(String tenFile);

}
