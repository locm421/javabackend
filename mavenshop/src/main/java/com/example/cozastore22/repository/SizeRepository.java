package com.example.cozastore22.repository;

import com.example.cozastore22.entity.SizeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SizeRepository extends JpaRepository<SizeEntity, Integer> {
}
