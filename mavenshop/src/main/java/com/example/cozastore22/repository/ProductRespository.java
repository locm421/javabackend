package com.example.cozastore22.repository;

import com.example.cozastore22.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRespository extends JpaRepository<ProductEntity, Integer> {
    List<ProductEntity> findByCategory_Id(int categoryId);
}
