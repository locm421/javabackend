package com.example.cozastore22.repository;

import com.example.cozastore22.entity.ProductDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDetailRepository extends JpaRepository<ProductDetailEntity, Integer> {
}
