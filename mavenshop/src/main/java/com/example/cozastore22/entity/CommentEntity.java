package com.example.cozastore22.entity;

import jakarta.persistence.*;

@Entity(name = "comment")
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String content;
    private String email;
    private String website;

    @ManyToOne
    @JoinColumn(name = "id_blog")
    private BlogEntity blogEntity;

    public CommentEntity(Long id, String name, String content, String email, String website, BlogEntity blogEntity) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.email = email;
        this.website = website;
        this.blogEntity = blogEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BlogEntity getBlogEntity() {
        return blogEntity;
    }

    public void setBlogEntity(BlogEntity blogEntity) {
        this.blogEntity = blogEntity;
    }

    @Override
    public String toString() {
        return "CommentEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", email='" + email + '\'' +
                ", website='" + website + '\'' +
                ", blogEntity=" + blogEntity +
                '}';
    }
}
