package com.example.cozastore22.entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity(name = "orderdetail")
public class OrderDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "id_product_detail")
    private ProductDetailEntity productDetailEntity;

    private double price;
    private int quantity;
    private Date createDate;

    public OrderDetailEntity(Long id, UserEntity userEntity, ProductDetailEntity productDetailEntity, double price, int quantity, Date createDate) {
        this.id = id;
        this.userEntity = userEntity;
        this.productDetailEntity = productDetailEntity;
        this.price = price;
        this.quantity = quantity;
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public ProductDetailEntity getProductDetailEntity() {
        return productDetailEntity;
    }

    public void setProductDetailEntity(ProductDetailEntity productDetailEntity) {
        this.productDetailEntity = productDetailEntity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "OrderDetailEntity{" +
                "id=" + id +
                ", userEntity=" + userEntity +
                ", productDetailEntity=" + productDetailEntity +
                ", price=" + price +
                ", quantity=" + quantity +
                ", createDate=" + createDate +
                '}';
    }
}
