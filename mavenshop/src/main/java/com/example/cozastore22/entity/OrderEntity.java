package com.example.cozastore22.entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private UserEntity userEntity;

    private double totalPrice;
    private Date createDate;

    public OrderEntity(Long id, UserEntity userEntity, double totalPrice, Date createDate) {
        this.id = id;
        this.userEntity = userEntity;
        this.totalPrice = totalPrice;
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id=" + id +
                ", userEntity=" + userEntity +
                ", totalPrice=" + totalPrice +
                ", createDate=" + createDate +
                '}';
    }
}
