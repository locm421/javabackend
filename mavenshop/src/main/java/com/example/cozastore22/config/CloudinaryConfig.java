package com.example.cozastore22.config;

import com.cloudinary.Cloudinary;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CloudinaryConfig {
    private final String CLOUD_NAME="dv5ptz3vr";
    private final String API_KEY="746129566456764";
    private final String API_SECRET="gu2fkAiD1M9jGFRPf67IwybTjxs";
    @Bean
    public Cloudinary cloudinary(){
        Map config = new HashMap<>();
        config.put("cloud_name", CLOUD_NAME);
        config.put("api_key", API_KEY);
        config.put("api_secret", API_SECRET);
        config.put("secure", true);
        return new Cloudinary(config);
    }
}
