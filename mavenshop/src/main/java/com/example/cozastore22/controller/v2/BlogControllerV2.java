package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.BlogEntity;
import com.example.cozastore22.payload.response.BaseResponse;
import com.example.cozastore22.payload.response.BlogResponse;
import com.example.cozastore22.service.v2.BlogServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/blogs")
public class BlogControllerV2 {

    @Autowired
    private BlogServiceV2 blogServiceV2;

    @GetMapping
    public ResponseEntity<?> getAllBlogs() {
        List<BlogEntity> blogs = blogServiceV2.getAllBlogs();

        List<BlogResponse> o = new ArrayList<>();
        for(BlogEntity item: blogs)
        {
            BlogResponse e = new BlogResponse();
            e.setId(item.getId());
            e.setCreateDate(item.getCreateDate());
            e.setUserName(item.getUserEntity().getEmail());
            o.add(e);
        }
        BaseResponse response = new BaseResponse();

        response.setData(o);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlogEntity> getBlogById(@PathVariable Long id) {
        BlogEntity blog = blogServiceV2.getBlogById(id);
        return ResponseEntity.ok(blog);
    }

    @PostMapping
    public ResponseEntity<BlogEntity> createBlog(@RequestBody BlogEntity blog) {
        BlogEntity createdBlog = blogServiceV2.createBlog(blog);
        return ResponseEntity.created(URI.create("/api/blogs/" + createdBlog.getId())).body(createdBlog);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BlogEntity> updateBlog(@PathVariable Long id, @RequestBody BlogEntity updatedBlog) {
        BlogEntity blog = blogServiceV2.updateBlog(id, updatedBlog);
        return ResponseEntity.ok(blog);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBlog(@PathVariable Long id) {
        blogServiceV2.deleteBlog(id);
        return ResponseEntity.noContent().build();
    }
}