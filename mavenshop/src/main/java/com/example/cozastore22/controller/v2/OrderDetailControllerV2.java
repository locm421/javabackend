package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.OrderDetailEntity;
import com.example.cozastore22.service.v2.OrderDetailServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/order-details")
public class OrderDetailControllerV2 {

    @Autowired
    private OrderDetailServiceV2 orderDetailServiceV2;

    @GetMapping
    public ResponseEntity<List<OrderDetailEntity>> getAllOrderDetails() {
        List<OrderDetailEntity> orderDetails = orderDetailServiceV2.getAllOrderDetails();
        return ResponseEntity.ok(orderDetails);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDetailEntity> getOrderDetailById(@PathVariable Long id) {
        OrderDetailEntity orderDetail = orderDetailServiceV2.getOrderDetailById(id);
        return ResponseEntity.ok(orderDetail);
    }

    @PostMapping
    public ResponseEntity<OrderDetailEntity> createOrderDetail(@RequestBody OrderDetailEntity orderDetail) {
        OrderDetailEntity createdOrderDetail = orderDetailServiceV2.createOrderDetail(orderDetail);
        return ResponseEntity.created(URI.create("/api/order-details/" + createdOrderDetail.getId()))
                .body(createdOrderDetail);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderDetailEntity> updateOrderDetail(@PathVariable Long id,
                                                               @RequestBody OrderDetailEntity updatedOrderDetail) {
        OrderDetailEntity orderDetail = orderDetailServiceV2.updateOrderDetail(id, updatedOrderDetail);
        return ResponseEntity.ok(orderDetail);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrderDetail(@PathVariable Long id) {
        orderDetailServiceV2.deleteOrderDetail(id);
        return ResponseEntity.noContent().build();
    }
}
