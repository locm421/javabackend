package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.CommentEntity;
import com.example.cozastore22.service.v2.CommentServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/comments")
public class CommentControllerV2 {

    @Autowired
    private CommentServiceV2 commentServiceV2;

    @GetMapping
    public ResponseEntity<List<CommentEntity>> getAllComments() {
        List<CommentEntity> comments = commentServiceV2.getAllComments();
        return ResponseEntity.ok(comments);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommentEntity> getCommentById(@PathVariable Long id) {
        CommentEntity comment = commentServiceV2.getCommentById(id);
        return ResponseEntity.ok(comment);
    }

    @PostMapping
    public ResponseEntity<CommentEntity> createComment(@RequestBody CommentEntity comment) {
        CommentEntity createdComment = commentServiceV2.createComment(comment);
        return ResponseEntity.created(URI.create("/api/comments/" + createdComment.getId())).body(createdComment);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CommentEntity> updateComment(@PathVariable Long id, @RequestBody CommentEntity updatedComment) {
        CommentEntity comment = commentServiceV2.updateComment(id, updatedComment);
        return ResponseEntity.ok(comment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(@PathVariable Long id) {
        commentServiceV2.deleteComment(id);
        return ResponseEntity.noContent().build();
    }
}
