package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.CountryEntity;
import com.example.cozastore22.service.v2.CountryServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/countries")
public class CountryControllerV2 {

    @Autowired
    private CountryServiceV2 countryServiceV2;

    @GetMapping
    public ResponseEntity<List<CountryEntity>> getAllCountries() {
        List<CountryEntity> countries = countryServiceV2.getAllCountries();
        return ResponseEntity.ok(countries);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CountryEntity> getCountryById(@PathVariable Long id) {
        CountryEntity country = countryServiceV2.getCountryById(id);
        return ResponseEntity.ok(country);
    }

    @PostMapping
    public ResponseEntity<CountryEntity> createCountry(@RequestBody CountryEntity country) {
        CountryEntity createdCountry = countryServiceV2.createCountry(country);
        return ResponseEntity.created(URI.create("/api/countries/" + createdCountry.getId())).body(createdCountry);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CountryEntity> updateCountry(@PathVariable Long id, @RequestBody CountryEntity updatedCountry) {
        CountryEntity country = countryServiceV2.updateCountry(id, updatedCountry);
        return ResponseEntity.ok(country);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCountry(@PathVariable Long id) {
        countryServiceV2.deleteCountry(id);
        return ResponseEntity.noContent().build();
    }
}
