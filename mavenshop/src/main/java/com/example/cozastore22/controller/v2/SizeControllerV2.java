package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.SizeEntity;
import com.example.cozastore22.service.v2.SizeServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/sizes")
public class SizeControllerV2 {

    @Autowired
    private SizeServiceV2 sizeServiceV2;

    @GetMapping
    public ResponseEntity<List<SizeEntity>> getAllSizes() {
        List<SizeEntity> sizes = sizeServiceV2.getAllSizes();
        return ResponseEntity.ok(sizes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SizeEntity> getSizeById(@PathVariable int id) {
        SizeEntity size = sizeServiceV2.getSizeById(id);
        return ResponseEntity.ok(size);
    }

    @PostMapping
    public ResponseEntity<SizeEntity> createSize(@RequestBody SizeEntity size) {
        SizeEntity createdSize = sizeServiceV2.createSize(size);
        return ResponseEntity.created(URI.create("/api/sizes/" + createdSize.getId())).body(createdSize);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SizeEntity> updateSize(@PathVariable int id, @RequestBody SizeEntity updatedSize) {
        SizeEntity size = sizeServiceV2.updateSize(id, updatedSize);
        return ResponseEntity.ok(size);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSize(@PathVariable int id) {
        sizeServiceV2.deleteSize(id);
        return ResponseEntity.noContent().build();
    }
}
