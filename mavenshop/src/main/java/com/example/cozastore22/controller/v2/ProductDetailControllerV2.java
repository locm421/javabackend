package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.ProductDetailEntity;
import com.example.cozastore22.service.v2.ProductDetailServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/product-details")
public class ProductDetailControllerV2 {

    @Autowired
    private ProductDetailServiceV2 productDetailServiceV2;

    @GetMapping
    public ResponseEntity<List<ProductDetailEntity>> getAllProductDetails() {
        List<ProductDetailEntity> productDetails = productDetailServiceV2.getAllProductDetails();
        return ResponseEntity.ok(productDetails);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDetailEntity> getProductDetailById(@PathVariable int id) {
        ProductDetailEntity productDetail = productDetailServiceV2.getProductDetailById(id);
        return ResponseEntity.ok(productDetail);
    }

    @PostMapping
    public ResponseEntity<ProductDetailEntity> createProductDetail(@RequestBody ProductDetailEntity productDetail) {
        ProductDetailEntity createdProductDetail = productDetailServiceV2.createProductDetail(productDetail);
        return ResponseEntity.created(URI.create("/api/product-details/" + createdProductDetail.getIdProductDetail()))
                .body(createdProductDetail);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDetailEntity> updateProductDetail(@PathVariable int id,
                                                                   @RequestBody ProductDetailEntity updatedProductDetail) {
        ProductDetailEntity productDetail = productDetailServiceV2.updateProductDetail(id, updatedProductDetail);
        return ResponseEntity.ok(productDetail);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProductDetail(@PathVariable int id) {
        productDetailServiceV2.deleteProductDetail(id);
        return ResponseEntity.noContent().build();
    }
}
