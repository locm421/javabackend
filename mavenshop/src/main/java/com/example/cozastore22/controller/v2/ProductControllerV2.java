package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.ProductEntity;
import com.example.cozastore22.payload.response.BaseResponse;
import com.example.cozastore22.payload.response.ProductResponse;
import com.example.cozastore22.service.imp.CloudImagesSerivcesImp;
import com.example.cozastore22.service.v2.ProductServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/products")
public class ProductControllerV2 {

    @Autowired
    private CloudImagesSerivcesImp cloudImagesSerivcesImp;
    @Autowired
    private ProductServiceV2 productServiceV2;

    @GetMapping("")
    public ResponseEntity<?> getAllProducts() {
        List<ProductResponse> products = productServiceV2.getAllProducts();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @GetMapping("/trouser")
    public ResponseEntity<?> getAllTrouser(){
        List<ProductResponse> products = productServiceV2.getAllProductCategory(3);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
    @GetMapping("/shirt")
    public ResponseEntity<?> getAllShirt(){
        List<ProductResponse> products = productServiceV2.getAllProductCategory(1);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
    @GetMapping("/dress")
    public ResponseEntity<?> getAllDress(){
        List<ProductResponse> products = productServiceV2.getAllProductCategory(7);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
    @GetMapping("/accessory")
    public ResponseEntity<?> getAllAccessory(){
        List<ProductResponse> products = productServiceV2.getAllProductCategory(2);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @GetMapping("/getonlyid/{id}")
    public ResponseEntity<ProductEntity> getProductById(@PathVariable int id) {
        ProductEntity product = productServiceV2.getProductById(id);
        return ResponseEntity.ok(product);
    }

    @PostMapping
    public ResponseEntity<Map> uploadImage(@RequestParam("image") MultipartFile file, @RequestParam double price, @RequestParam String title, @RequestParam int id_category){

        Map data = this.cloudImagesSerivcesImp.upload(file);
        String img_url = (String) data.get("secure_url");
        System.out.println(img_url);
        productServiceV2.insertProduct(title,price,id_category, img_url);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PutMapping("/updateproduct/{id}")
    public ResponseEntity<ProductEntity> updateProduct(@PathVariable int id, @RequestBody ProductEntity updatedProduct) {
        ProductEntity product = productServiceV2.updateProduct(id, updatedProduct);
        return ResponseEntity.ok(product);
    }

    @DeleteMapping("/deleteproduct/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable int id) {
        productServiceV2.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
}
