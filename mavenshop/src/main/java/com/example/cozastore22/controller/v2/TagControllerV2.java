package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.TagEntity;
import com.example.cozastore22.service.v2.TagServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/tags")
public class TagControllerV2 {

    @Autowired
    private TagServiceV2 tagServiceV2;

    @GetMapping
    public ResponseEntity<List<TagEntity>> getAllTags() {
        List<TagEntity> tags = tagServiceV2.getAllTags();
        return ResponseEntity.ok(tags);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagEntity> getTagById(@PathVariable Long id) {
        TagEntity tag = tagServiceV2.getTagById(id);
        return ResponseEntity.ok(tag);
    }

    @PostMapping
    public ResponseEntity<TagEntity> createTag(@RequestBody TagEntity tag) {
        TagEntity createdTag = tagServiceV2.createTag(tag);
        return ResponseEntity.created(URI.create("/api/tags/" + createdTag.getId())).body(createdTag);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TagEntity> updateTag(@PathVariable Long id, @RequestBody TagEntity updatedTag) {
        TagEntity tag = tagServiceV2.updateTag(id, updatedTag);
        return ResponseEntity.ok(tag);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTag(@PathVariable Long id) {
        tagServiceV2.deleteTag(id);
        return ResponseEntity.noContent().build();
    }
}
