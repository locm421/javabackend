package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.ColorEntity;
import com.example.cozastore22.service.v2.ColorServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/colors")
public class ColorControllerV2 {

    @Autowired
    private ColorServiceV2 colorServiceV2;

    @GetMapping
    public ResponseEntity<List<ColorEntity>> getAllColors() {
        List<ColorEntity> colors = colorServiceV2.getAllColors();
        return ResponseEntity.ok(colors);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ColorEntity> getColorById(@PathVariable int id) {
        ColorEntity color = colorServiceV2.getColorById(id);
        return ResponseEntity.ok(color);
    }

    @PostMapping
    public ResponseEntity<ColorEntity> createColor(@RequestBody ColorEntity color) {
        ColorEntity createdColor = colorServiceV2.createColor(color);
        return ResponseEntity.created(URI.create("/api/colors/" + createdColor.getId())).body(createdColor);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ColorEntity> updateColor(@PathVariable int id, @RequestBody ColorEntity updatedColor) {
        ColorEntity color = colorServiceV2.updateColor(id, updatedColor);
        return ResponseEntity.ok(color);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteColor(@PathVariable int id) {
        colorServiceV2.deleteColor(id);
        return ResponseEntity.noContent().build();
    }
}
