package com.example.cozastore22.controller;

import com.example.cozastore22.controller.v2.ProductControllerV2;
import com.example.cozastore22.payload.response.BaseResponse;
import com.example.cozastore22.payload.response.ProductResponse;
import com.example.cozastore22.service.FileStorageService;
import com.example.cozastore22.service.ProductService;
import com.example.cozastore22.service.imp.FileStorageServiceImp;
import com.example.cozastore22.service.imp.ProducServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin
//@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProducServiceImp producServiceImp;

    @Autowired
    private FileStorageServiceImp fileStorageServiceImp;

    @GetMapping("")
    public ResponseEntity<?>getProduct(){


        List<ProductResponse> products = producServiceImp.getAllProducts();
        BaseResponse baseResponse = new BaseResponse();

        baseResponse.setData(products);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }



    @PostMapping("")
    public ResponseEntity<?>insertProduct(@RequestParam MultipartFile file){//,@RequestParam String title,@RequestParam double price, @RequestParam int idCategory){

       // System.out.println("title: " + title);
        fileStorageServiceImp.saveFile(file);
        //producServiceImp.insertProduct(title,price,idCategory);
        return new ResponseEntity<>("Product Insert", HttpStatus.OK);
    }
    @GetMapping("/{tenfile}")
    public ResponseEntity<?> dowloadProductFile(@PathVariable String tenfile){
        Resource  resource = producServiceImp.downloadProductFile(tenfile);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + tenfile + "\"").body(resource);
    }

}
