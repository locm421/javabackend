package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.RoleEntity;
import com.example.cozastore22.service.v2.RoleServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/roles")
public class RoleControllerV2 {

    @Autowired
    private RoleServiceV2 roleServiceV2;

    @GetMapping
    public ResponseEntity<List<RoleEntity>> getAllRoles() {
        List<RoleEntity> roles = roleServiceV2.getAllRoles();
        return ResponseEntity.ok(roles);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RoleEntity> getRoleById(@PathVariable int id) {
        RoleEntity role = roleServiceV2.getRoleById(id);
        return ResponseEntity.ok(role);
    }

    @PostMapping
    public ResponseEntity<RoleEntity> createRole(@RequestBody RoleEntity role) {
        RoleEntity createdRole = roleServiceV2.createRole(role);
        return ResponseEntity.created(URI.create("/api/roles/" + createdRole.getId())).body(createdRole);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RoleEntity> updateRole(@PathVariable int id, @RequestBody RoleEntity updatedRole) {
        RoleEntity role = roleServiceV2.updateRole(id, updatedRole);
        return ResponseEntity.ok(role);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRole(@PathVariable int id) {
        roleServiceV2.deleteRole(id);
        return ResponseEntity.noContent().build();
    }
}
