package com.example.cozastore22.controller.v2;

import com.example.cozastore22.entity.UserEntity;
import com.example.cozastore22.payload.response.BaseResponse;
import com.example.cozastore22.payload.response.CategoryResponse;
import com.example.cozastore22.payload.response.UserResponse;
import com.example.cozastore22.service.v2.UserServiceV2;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
@RequestMapping("/api/v2/users")
public class UserControllerV2 {

    @Autowired
    private UserServiceV2 userServiceV2;

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        List<UserResponse> users = userServiceV2.getAllUsers();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setData(users);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getUserById(@PathVariable int id) {
        UserEntity user = userServiceV2.getUserById(id);
        return ResponseEntity.ok(user);
    }

    @PostMapping
    public ResponseEntity<UserEntity> createUser(@RequestBody UserEntity user) {
        UserEntity createdUser = userServiceV2.createUser(user);
        return ResponseEntity.created(URI.create("/api/users/" + createdUser.getId())).body(createdUser);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserEntity> updateUser(@PathVariable int id, @RequestBody UserEntity updatedUser) {
        UserEntity user = userServiceV2.updateUser(id, updatedUser);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        userServiceV2.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
